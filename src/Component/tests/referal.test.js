import React from "react";
import waitUntil from "async-wait-until";
import { mount, shallow } from "enzyme";
import Referal, { Referal as classReferal } from "../referal";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

global.fetch = jest.fn(url => {
  if (url.includes("ref/ReferredUserList")) {
    return Promise.resolve({
      json: () =>
        Promise.resolve({
          total_credits: 25,
          total_people: 3,
          list_of_people: ["a", "b", "c"]
        })
    });
  } else if (url.includes("firebase/CreateDynamicLink")) {
    return Promise.resolve({
      json: () =>
        Promise.resolve({
          shortLink: "https://hdcric.page.link/g79NQLtmD7y6xts19"
        })
    });
  }
  return null;
});

describe("Referal Page Tests", () => {
  let wrapper = shallow(<Referal />).dive();

  beforeEach(() => {
    fetch.mockClear();
    // fetchMock.mockIf(/^https?:\/\/dev.hyperdart.com.*$/, req => {
    //   if (req.url.endsWith("ref/ReferredUserList")) {
    //     return {
    //       status: 200,
    //       body: JSON.stringify({
    //         total_credits: 25,
    //         total_people: 3,
    //         list_of_people: ["a", "b", "c"]
    //       })
    //     };
    //   } else if (
    //     req.url.endsWith(
    //       "/firebase/CreateDynamicLink?link=https://dev.hyperdart.com&ref=manav"
    //     )
    //   ) {
    //     return JSON.parse({
    //       shortLink: "https://hdcric.page.link/g79NQLtmD7y6xts19"
    //     });
    //   } else {
    //     return {
    //       status: 404,
    //       body: "Not Found"
    //     };
    //   }
    // });
  });

  it("renders properly", () => {
    expect(wrapper.find("#root_paper").children().length).toEqual(7);
  });

  it("invite line has proper text", () => {
    expect(wrapper.find("#invite_line").exists()).toEqual(true);
    expect(
      wrapper
        .find("#invite_line")
        .render()
        .text()
        .includes("Share")
    ).toEqual(true);
  });

  it("invite heading is proper", () => {
    expect(wrapper.find("#invite_heading").exists()).toEqual(true);
    expect(
      wrapper
        .find("#invite_heading")
        .render()
        .text()
    ).toEqual("Invite your friends to Hyperdart Cricket App");
  });

  it("invite link is proper format", async () => {
  wrapper = shallow(<Referal/>)
  const classApp = wrapper.find(classReferal).dive()
    await waitUntil(()=> classApp.state().invite_link!='test link')
    expect(
      classApp.state().invite_link.includes("hdcric.page.link")
    ).toEqual(true);
  });

  it("api test", async () => {
    wrapper = shallow(<Referal />);
    const classApp = wrapper.find(classReferal).dive();
    await waitUntil(() => classApp.state().totalCredits != 0);
    expect(classApp.state().totalCredits).toEqual(25);
  });
});
