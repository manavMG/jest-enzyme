import React, { Component } from "react";
import { HDComponent } from "hdcomp";
// import { HDComponent } from "hdcomp";
import {
  Paper,
  Typography,
  Button,
  List,
  ListItem,
  Grid
} from "@material-ui/core";
import FileCopyOutlinedIcon from "@material-ui/icons/FileCopyOutlined";
import { withStyles } from "@material-ui/core/styles";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
// import Referal_img from "./../assets/pie.png";
import { ReferrerList } from "./serviceClass";
import { Helmet } from "react-helmet";

const styles = theme => ({
  exp: {
    overflowY: "hidden",
    maxHeight: "60px",
    "&:hover": {
      overflowY: "scroll"
    }
  },
  panel_clp: {
    maxHeight: "180px",
    maxWidth: "350px",
    margin: "0 auto"
  },
  panel_exp: {
    maxWidth: "350px",
    margin: "0 auto"
  }
});

class Referal extends HDComponent {
  constructor(props) {
    super(props);

    this.setHDState({
      starEnabled: false,
      isBookmarked: false,
      header: { primary: "Referral" }
    });

    this.state = {
      total_users: 0,
      invite_link: "test link",
      all_unique_users: [],
      totalCredits: 0,
      collapsed: true
    };
    // this.generateInviteLink();
    // this.getReferrerList();
  }

  async componentDidMount() {
    this.generateInviteLink();
    this.getReferrerList();
  }

  copyToClipBoard = () => {
    const el = document.createElement("textarea");
    el.innerText = this.state.invite_link;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
  };

  generateInviteLink = () => {
    fetch(
      "https://dev.hyperdart.com/nodeapi/firebase/CreateDynamicLink?link=https://dev.hyperdart.com&ref=manav",
      { credentials: "include" }
    )
      .then(res => res.json())
      .then(data => {
        this.setState({
          invite_link: data.shortLink
        });
      })
      .catch(error => console.log(error));
  };

  getReferrerList = () => {
    ReferrerList("manav")
      .then(data => {
        console.log(data);
        this.setState({
          total_users: data.unique.length,
          all_unique_users: data.unique,
          totalCredits: data.total_credits
        });
      })
      .catch(err => console.log(err));
  };

  render() {
    const { classes, theme } = this.props;
    // document.title="My Referral Page";
    // var meta=createElement("META")
    // document.getElementsByTagName('META')[3].content="Description for meta crawling"
    let display_list,
      one_arr = [];
    if (this.state.collapsed) {
      this.state.all_unique_users.map((el, i) => {
        if (i < 2 && i < this.state.all_unique_users.length) {
          one_arr.push(
            <ListItem>
              <Typography style={{ margin: "0 auto" }}>{el}</Typography>
            </ListItem>
          );
        }
      });

      // for (let i = 0; i < 2 && i < this.state.all_unique_users.length; i++) {
      //   one_arr.push(
      //     <ListItem>
      //       <Typography style={{ margin: "0 0" }}>
      //         {this.state.all_unique_users[i]}
      //       </Typography>
      //     </ListItem>
      //   );
      // }
    } else {
      one_arr = this.state.all_unique_users.map(element => (
        <ListItem>
          <Typography style={{ margin: "0 auto" }}>{element}</Typography>
        </ListItem>
      ));
    }
    if (this.state.total_users == 0) {
      display_list = (
        <div>
          <Typography
            style={{
              color: theme.palette.primary.light,
              paddingTop: "12px",
              paddingBottom: "6px",
              fontSize: "12px",
              paddingLeft: "26px",
              paddingRight: "26px",
              margin: "0 auto",
              textAlign: "center"
            }}
          >
            Your referred people will be shown here
          </Typography>
        </div>
      );
    } else {
      display_list = (
        <div>
          <List
            dense="true"
            style={{ paddingLeft: "10px", paddingRight: "10px" }}
          >
            {one_arr}
          </List>
          <Typography
            style={{
              color: theme.palette.primary.main,
              fontSize: "12px",
              textAlign: "center",
              paddingBottom: "6px"
            }}
          >
            {this.state.collapsed ? (
              this.state.total_users <= 2 ? null : (
                <div
                  style={{
                    cursor: "pointer",
                    display: "inline-block"
                  }}
                  onClick={() => {
                    this.setState({
                      collapsed: !this.state.collapsed
                    });
                  }}
                >
                  +{this.state.total_users - 2} more
                </div>
              )
            ) : (
              <div
                style={{
                  cursor: "pointer",
                  display: "inline-block"
                }}
                onClick={() => {
                  this.setState({
                    collapsed: !this.state.collapsed
                  });
                }}
              >
                Show Less
              </div>
            )}
          </Typography>
        </div>
      );
    }

    return (
      <Paper
        id="root_paper"
        style={{
          maxWidth: "720px",
          margin: "0px auto",
          boxShadow: "none",
          paddingBottom: "20px"
        }}
      >
        <Helmet>
          <meta name="description" content="meta inside helmet" />
        </Helmet>
        {/* <img
          src={Referal_img}
          alt="referal image"
          style={{
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
            maxWidth: "100%"
          }}
        /> */}

        <Typography
          // variant="h2"
          id="invite_heading"
          align="center"
          style={{
            marginBlockEnd: "3px",
            marginBlockStart: "6px",
            paddingLeft: "20px",
            paddingRight: "20px",
            fontSize: "20px",
            fontWeight: "bold"
          }}
        >
          Invite your friends to Hyperdart Cricket App
        </Typography>

        <Typography
          id="invite_line"
          style={{
            textAlign: "center",
            marginLeft: "20px",
            marginRight: "20px",
            marginTop: "20px"
          }}
        >
          Share the app with your friends using the link below and when they
          sign up you both get amazing benefits
        </Typography>

        <Paper
          elevation="3"
          style={{
            maxWidth: "350px",
            display: "flex",
            marginLeft: "auto",
            marginRight: "auto",
            marginBlockEnd: "20px",
            marginBlockStart: "20px",
            paddingBottom: "11px",
            paddingTop: "11px"
          }}
        >
          <Typography
            id="invite_box"
            align="center"
            style={{
              align: "center",
              paddingLeft: "5px",
              paddingRight: "5px",
              fontSize: "14px"
            }}

            // Share function onclick to be added here
          >
            {this.state.invite_link}
          </Typography>
          <Button
            style={{
              verticalAlign: "middle",
              minWidth: "22px",
              height: "22px",
              marginLeft: "10px",
              padding: "0px"
            }}
          >
            <FileCopyOutlinedIcon
              style={{
                width: "20px",
                height: "20px"
              }}
              onClick={() => {
                this.copyToClipBoard();
              }}
            />
          </Button>
        </Paper>
        <Button
          style={{
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
            marginBlockEnd: "20px"
          }}
          variant="contained"
          color="primary"

          // Share function onclick to be added here
        >
          <Typography>Refer Now!</Typography>
        </Button>

        <Grid
          container
          item
          xs={12}
          spacing={3}
          style={{
            maxWidth: "350px",
            marginLeft: "auto",
            marginRight: "auto",
            padding: "0",
            paddingTop: "20px",
            paddingBottom: "41px"
          }}
        >
          <Grid item xs={6} style={{ padding: "0", paddingRight: "6px" }}>
            <Paper>
              <Typography
                style={{
                  fontSize: "17px",
                  color: theme.palette.primary.main,
                  paddingTop: "6px",
                  textAlign: "center",
                  paddingBottom: "6px"
                }}
              >
                Users Referred
              </Typography>

              <Typography
                style={{
                  fontSize: "17px",
                  paddingTop: "6px",
                  textAlign: "center",
                  paddingBottom: "6px"
                }}
              >
                {this.state.total_users}
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={6} style={{ padding: "0", paddingLeft: "6px" }}>
            <Paper>
              <Typography
                style={{
                  fontSize: "17px",
                  color: theme.palette.primary.main,
                  paddingTop: "6px",
                  textAlign: "center",
                  paddingBottom: "6px"
                }}
              >
                Credits Earned
              </Typography>
              <Typography
                style={{
                  fontSize: "17px",
                  paddingTop: "6px",
                  textAlign: "center",
                  paddingBottom: "6px"
                }}
              >
                {this.state.totalCredits}
              </Typography>
            </Paper>
          </Grid>
        </Grid>

        <Paper
          className={
            this.state.collapsed ? classes.panel_clp : classes.panel_exp
          }
        >
          <Typography
            style={{
              fontSize: "17px",
              color: theme.palette.primary.main,
              paddingTop: "6px",
              textAlign: "center"
            }}
          >
            Users Referred by me
          </Typography>
          {display_list}
        </Paper>
      </Paper>
    );
  }
}
// export class {Referal}
// export class Referal ...
export { Referal };
export default withStyles(styles, { withTheme: true })(Referal);
