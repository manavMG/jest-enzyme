import CONFIG from "./config";

export const ReferrerList = name => {
  return fetch(CONFIG.referList + name, { credentials: "include" })
    .then(res => res.json())
    .then(data => {
      let unique = data.list_of_people
      let total_credits = data.total_credits
      
      return {unique,total_credits}
    })
    .catch(err => console.log(err));
};
