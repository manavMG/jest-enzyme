# jest-enzyme-crl

> 

[![NPM](https://img.shields.io/npm/v/jest-enzyme-crl.svg)](https://www.npmjs.com/package/jest-enzyme-crl) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save jest-enzyme-crl
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'jest-enzyme-crl'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

 © [](https://github.com/)
